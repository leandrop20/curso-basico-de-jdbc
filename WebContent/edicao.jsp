<%@page import="br.edu.devmedia.jdbc.dto.EstadoDTO"%>
<%@page import="br.edu.devmedia.jdbc.dto.PessoaDTO"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Home</title>
</head>
<body>
	<form action="pessoa?acao=atualizar" method="post">
		<fieldset>
			<legend>Pessoa</legend>
			<%
				String msg = (String) request.getAttribute("msg");
				PessoaDTO pessoaDTO = (PessoaDTO) request.getAttribute("pessoaDTO");
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			%>
			<%= msg != null ? msg : "" %><br/><br/>
			<input type="hidden" name="id" value="<%= pessoaDTO.getId() %>" />
			<input type="hidden" name="idEndereco" value="<%= pessoaDTO.getEnderecoDTO().getId() %>" />
			<input type="hidden" name="idEstado" value="<%= pessoaDTO.getEnderecoDTO().getEstadoDTO().getId() %>" />
			<table>
				<tr>
					<td>Nome:</td>
					<td><input type="text" name="nome" value="<%= pessoaDTO.getNome() %>" /></td>
				</tr>
				<tr>
					<td>CPF:</td>
					<td><input type="text" name="cpf" value="<%= pessoaDTO.getCpf() %>" /></td>
				<tr/>
				<tr>
					<td>Dt. Nasc:</td>
					<td><input type="text" name="dtNasc" value="<%= dateFormat.format(pessoaDTO.getDt_nasc()) %>" /></td>
				</tr>
			</table>
		</fieldset>
		<fieldset>
			<legend>Sexo</legend>
			<table>
				<tr>
					<td>Sexo:</td>
					<td>
						<% if (pessoaDTO.getSexo() == 'M') { %>
							<input type="radio" name="sexo" value="M" checked /> Masculino
							<input type="radio" name="sexo" value="F" /> Feminino
						<% } else { %>
							<input type="radio" name="sexo" value="M" /> Masculino
							<input type="radio" name="sexo" value="F" checked /> Feminino
						<% } %>
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset>
			<legend>Endere�o</legend>
			<table>
				<tr>
					<td>Logradouro:</td>
					<td><input type="text" name="logradouro" value="<%= pessoaDTO.getEnderecoDTO().getLogradouro() %>" /></td>
				</tr>
				<tr>
					<td>Bairro:</td>
					<td><input type="text" name="bairro" value="<%= pessoaDTO.getEnderecoDTO().getBairro() %>" /></td>
				</tr>
				<tr>
					<td>Cidade:</td>
					<td><input type="text" name="cidade" value="<%= pessoaDTO.getEnderecoDTO().getCidade() %>" /></td>
				</tr>
				<tr>
					<td>Numero:</td>
					<td><input type="text" name="numero" value="<%= pessoaDTO.getEnderecoDTO().getNumero() %>" /></td>
				</tr>
				<tr>
					<td>CEP:</td>
					<td><input type="text" name="cep" value="<%= pessoaDTO.getEnderecoDTO().getCep() %>" /></td>
				</tr>
				<tr>
					<td>UF:</td>
					<td>
						<select name="uf">
							<%
								List<EstadoDTO> listaEstados = (List<EstadoDTO>) session.getAttribute("listaEstados");
								for (EstadoDTO estadoDTO : listaEstados) {
									if (estadoDTO.getId().equals(pessoaDTO.getEnderecoDTO().getEstadoDTO().getId())) {
										%>
										<option value="<%= estadoDTO.getId() %>" selected><%= estadoDTO.getDescricao() %></option>
										<%
									} else {
										%>
										<option value="<%= estadoDTO.getId() %>"><%= estadoDTO.getDescricao() %></option>
										<%
									}
								}
							%>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		<input type="submit" value="Atualizar" />
		<input type="reset" value="Limpar" />
	</form>
</body>
</html>