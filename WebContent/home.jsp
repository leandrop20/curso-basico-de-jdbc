<%@page import="br.edu.devmedia.jdbc.dto.EstadoDTO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Home</title>
</head>
<body>
	<form action="pessoa?acao=cadastrar" method="post">
		<fieldset>
			<legend>Pessoa</legend>
			<%
				String msg = (String) request.getAttribute("msg");
			%>
			<%= msg != null ? msg : "" %><br/><br/>
			<table>
				<tr>
					<td>Nome:</td>
					<td><input type="text" name="nome" /></td>
				</tr>
				<tr>
					<td>CPF:</td>
					<td><input type="text" name="cpf" /></td>
				<tr/>
				<tr>
					<td>Dt. Nasc:</td>
					<td><input type="text" name="dtNasc" /></td>
				</tr>
			</table>
		</fieldset>
		<fieldset>
			<legend>Sexo</legend>
			<table>
				<tr>
					<td>Sexo:</td>
					<td>
						<input type="radio" name="sexo" value="M" checked /> Masculino
						<input type="radio" name="sexo" value="F" /> Feminino
					</td>
				</tr>
			</table>
		</fieldset>
		<fieldset>
			<legend>Endere�o</legend>
			<table>
				<tr>
					<td>Logradouro:</td>
					<td><input type="text" name="logradouro" /></td>
				</tr>
				<tr>
					<td>Bairro:</td>
					<td><input type="text" name="bairro" /></td>
				</tr>
				<tr>
					<td>Cidade:</td>
					<td><input type="text" name="cidade" /></td>
				</tr>
				<tr>
					<td>Numero:</td>
					<td><input type="text" name="numero" /></td>
				</tr>
				<tr>
					<td>CEP:</td>
					<td><input type="text" name="cep" /></td>
				</tr>
				<tr>
					<td>UF:</td>
					<td>
						<select name="uf">
							<%
								List<EstadoDTO> listaEstados = (List<EstadoDTO>) session.getAttribute("listaEstados");
								for (EstadoDTO estadoDTO : listaEstados) {
							%>
									<option value="<%= estadoDTO.getId() %>"><%= estadoDTO.getDescricao() %></option>
							<%
								}
							%>
						</select>
					</td>
				</tr>
			</table>
		</fieldset>
		<input type="submit" value="Cadastrar" />
		<input type="reset" value="Limpar" />
		<button type="button" onclick="window.location='pessoa?acao=listagem'">Listagem</button>
	</form>
</body>
</html>