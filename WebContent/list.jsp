<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.edu.devmedia.jdbc.dto.PessoaDTO" %>
<%@page import="java.text.DateFormat" %>
<%@page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lista de Pessoas</title>
</head>
<body>
	<%
		String msg = (String) request.getAttribute("msg");
	%>
	<%= msg != null ? msg : "" %><br/><br/>
	<table border="1" cellpadding="5" cellspacing="0" width="500">
		<tr style="background-color: #cccccc;" align="center">
			<td>id</td>
			<td>Nome</td>
			<td>Dt Nasc.</td>
			<td>Cidade</td>
			<td>Bairro</td>
			<td>Uf:</td>
			<td>A��es</td>
		</tr>
		<%
			List<PessoaDTO> listaPessoas = (List<PessoaDTO>) request.getAttribute("listaPessoas");
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			for (PessoaDTO pessoaDTO : listaPessoas) {
		%>
				<tr>
					<td><%= pessoaDTO.getId() %></td>
					<td><%= pessoaDTO.getNome() %></td>
					<td><%= dateFormat.format(pessoaDTO.getDt_nasc()) %></td>
					<td><%= pessoaDTO.getEnderecoDTO().getCidade() %></td>
					<td><%= pessoaDTO.getEnderecoDTO().getBairro() %></td>
					<td><%= pessoaDTO.getEnderecoDTO().getEstadoDTO().getUf() %></td>
					<td>
						<a href="pessoa?acao=editar&id=<%= pessoaDTO.getId() %>">Editar</a>
						<a href="pessoa?acao=remover&idPessoa=<%= pessoaDTO.getId() %>&idEndereco=<%= pessoaDTO.getEnderecoDTO().getId() %>">Remover</a>
					</td>
				</tr>
		<%
			}
		%>
	</table>
	<a href="home.jsp">Novo Cadastro</a>
</body>
</html>