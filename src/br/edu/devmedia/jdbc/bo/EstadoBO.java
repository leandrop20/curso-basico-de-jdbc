package br.edu.devmedia.jdbc.bo;

import java.util.ArrayList;
import java.util.List;

import br.edu.devmedia.jdbc.Exception.NegocioException;
import br.edu.devmedia.jdbc.dao.EstadoDAO;
import br.edu.devmedia.jdbc.dto.EstadoDTO;

public class EstadoBO {

	public List<EstadoDTO> listEstados() throws NegocioException {
		List<EstadoDTO> lista = new ArrayList<EstadoDTO>();
		try {
			EstadoDAO estadoDAO = new EstadoDAO();
			lista = estadoDAO.listEstados();
		} catch (Exception e) {
			throw new NegocioException(e.getMessage(), e);
		}
		return lista;
	}
	
}
