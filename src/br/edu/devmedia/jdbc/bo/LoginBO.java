package br.edu.devmedia.jdbc.bo;

import br.edu.devmedia.jdbc.Exception.NegocioException;
import br.edu.devmedia.jdbc.dao.LoginDAO;
import br.edu.devmedia.jdbc.dto.LoginDTO;

public class LoginBO {
	
	public boolean logar(LoginDTO loginDTO) throws NegocioException {
		boolean resultado = false;
		try {
			if (loginDTO.getLogin() == null || "".equals(loginDTO.getLogin())) {
				throw new NegocioException("Login obrigatório!");
			} else if (loginDTO.getSenha() == null || "".equals(loginDTO.getSenha())) {
				throw new NegocioException("Senha obrigatório!");
			} else {
				LoginDAO loginDAO = new LoginDAO();
				resultado = loginDAO.logar(loginDTO);
			}
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
		return resultado;
	}
}
