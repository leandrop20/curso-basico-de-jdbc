package br.edu.devmedia.jdbc.bo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import br.edu.devmedia.jdbc.Exception.NegocioException;
import br.edu.devmedia.jdbc.Exception.ValidacaoException;
import br.edu.devmedia.jdbc.dao.PessoaDAO;
import br.edu.devmedia.jdbc.dto.EnderecoDTO;
import br.edu.devmedia.jdbc.dto.PessoaDTO;

public class PessoaBO {

	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
	
	public void cadastrar(PessoaDTO pessoaDTO) throws NegocioException {
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoaDAO.inserir(pessoaDTO);
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
	}
	
	public void atualizar(PessoaDTO pessoaDTO) throws NegocioException {
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoaDAO.atualizar(pessoaDTO);
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
	}
	
	public void remover(Integer id, Integer enderecoID) throws NegocioException {
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoaDAO.remover(id);
			pessoaDAO.removerEndereco(enderecoID);
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
	}
	
	public boolean removeTudo() {
		PessoaDAO pessoaDAO = new PessoaDAO();
		return pessoaDAO.removeTudo();
	}
	
	public PessoaDTO buscaPorId(Integer id) throws NegocioException {
		PessoaDTO pessoaDTO = null;
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoaDTO = pessoaDAO.buscarPorId(id);
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
		return pessoaDTO;
	}
	
	public List<PessoaDTO> listagem() throws NegocioException {
		List<PessoaDTO> listaRetorno = null;
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			listaRetorno = pessoaDAO.listarTodos();
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
		return listaRetorno;
	}
	
	public String[][] listagem(List<Integer> idsPessoas) throws NegocioException {
		String[][] listaRetorno = null;
		SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yy");
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			List<PessoaDTO> lista = pessoaDAO.listarTodos();
			listaRetorno = new String[lista.size()][10];
			for (int i = 0; i < lista.size(); i++) {
				PessoaDTO pessoa = lista.get(i);
				EnderecoDTO enderecoDTO = pessoa.getEnderecoDTO();
				idsPessoas.add(pessoa.getId());
				listaRetorno[i][0] = pessoa.getId().toString();
				listaRetorno[i][1] = pessoa.getNome();
				listaRetorno[i][2] = pessoa.getCpf().toString();
				listaRetorno[i][3] = pessoa.getSexo() == 'M' ? "Masculino" : "Feminino";
				listaRetorno[i][4] = formatDate.format(pessoa.getDt_nasc());
				listaRetorno[i][5] = enderecoDTO.getLogradouro();
				listaRetorno[i][6] = enderecoDTO.getCep().toString();
				listaRetorno[i][7] = enderecoDTO.getEstadoDTO().getDescricao();
				listaRetorno[i][8] = "Editar";
				listaRetorno[i][9] = "Deletar";
			}
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
		return listaRetorno;
	}
	
	public String[][] listaConsulta(String nome, Long cpf, Character sexo, String orderBy) throws NegocioException {
		String[][] listaRetorno = null;
		SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yy");
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			List<PessoaDTO> lista = pessoaDAO.filtrarPessoa(nome, cpf, String.valueOf(sexo), orderBy);
			listaRetorno = new String[lista.size()][6];
			for (int i = 0; i < lista.size(); i++) {
				PessoaDTO pessoa = lista.get(i);
				listaRetorno[i][0] = pessoa.getId().toString();
				listaRetorno[i][1] = pessoa.getNome();
				listaRetorno[i][2] = pessoa.getCpf().toString();
				listaRetorno[i][3] = pessoa.getEndereco();
				listaRetorno[i][4] = pessoa.getSexo() == 'M' ? "Masculino" : "Feminino";
				listaRetorno[i][5] = formatDate.format(pessoa.getDt_nasc());
			}
		} catch (Exception e) {
			throw new NegocioException(e.getMessage());
		}
		return listaRetorno;
	}
	
	public boolean validaNome(String nome) throws ValidacaoException {
		boolean isValido = true;
		if (nome == null || nome.equals("")) {
			isValido = false;
			throw new ValidacaoException("Campo nome � obrigat�rio!");
		} else if (nome.length() > 30) {
			isValido = false;
			throw new ValidacaoException("M�ximo de 30 caracteres para o campo nome!");
		}
		return isValido;
	}
	
	public boolean validaCpf(String cpf) throws ValidacaoException {
		boolean isValido = true;
		if (cpf == null || cpf.equals("")) {
			isValido = false;
			throw new ValidacaoException("Campo cpf � obrigat�rio!");
		} else if (cpf.length() != 11) {
			isValido = false;
			throw new ValidacaoException("O cpf deve ter 11 dig�tos!");
		} else {
			char[] digitos = cpf.toCharArray();
			for (char digito : digitos) {
				if (!Character.isDigit(digito)) {
					isValido = false;
					throw new ValidacaoException("Apenas n�meros no campo cpf");
				}
			}
		}
		return isValido;
	}

	public boolean validaEndereco(EnderecoDTO enderecoDTO) throws ValidacaoException {
		boolean isValido = true;
		if (enderecoDTO.getLogradouro() == null || enderecoDTO.getLogradouro().equals("")) {
			isValido = false;
			throw new ValidacaoException("Campo Logradouro � obrigat�rio!");
		} else if (enderecoDTO.getLogradouro().length() > 50) {
			isValido = false;
			throw new ValidacaoException("M�ximo de 50 caracteres para o Logradouro!");
		} else if (enderecoDTO.getBairro() == null || enderecoDTO.getBairro().equals("")) {
			isValido = false;
			throw new ValidacaoException("Campo Bairro � obrigat�rio!");
		} else if (enderecoDTO.getNumero() == null || enderecoDTO.getNumero().equals("")) {
			isValido = false;
			throw new ValidacaoException("Campo Numero � obrigat�rio!");
		} else if (enderecoDTO.getCep() == null || enderecoDTO.getCep().equals("")) {
			isValido = false;
			throw new ValidacaoException("Campo Cep � obrigat�rio!");
		}
		return isValido;
	}
	
	public boolean validaDtNasc(String dtNasc) throws ValidacaoException {
		boolean isValido = true;
		if (dtNasc == null || dtNasc.equals("")) {
			isValido = false;
			throw new ValidacaoException("Campo dtNasc � obrigat�rio!");
		} else {
			try {
				dateFormat.parse(dtNasc);
			} catch (Exception e) {
				isValido = false;
				throw new ValidacaoException("Formato inv�lido de data!");
			}
		}
		return isValido;
	}
	
}
