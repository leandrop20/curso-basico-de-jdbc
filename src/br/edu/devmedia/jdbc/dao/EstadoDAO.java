package br.edu.devmedia.jdbc.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

import br.edu.devmedia.jdbc.ConexaoUtil;
import br.edu.devmedia.jdbc.Exception.PersistenciaException;
import br.edu.devmedia.jdbc.dto.EstadoDTO;

public class EstadoDAO {

	public List<EstadoDTO> listEstados() throws PersistenciaException {
		List<EstadoDTO> lista = new ArrayList<EstadoDTO>();
		
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "SELECT * FROM estados";
			
			PreparedStatement statement = (PreparedStatement) connection.prepareStatement(sql);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				EstadoDTO estadoDTO = new EstadoDTO();
				estadoDTO.setId(result.getInt("id"));
				estadoDTO.setUf(result.getString("uf"));
				estadoDTO.setDescricao(result.getString("descricao"));
				lista.add(estadoDTO);
			}
			connection.close();
		} catch (Exception e) {
			throw new PersistenciaException(e.getMessage(), e);
		}
		return lista;
	}
	
}
