package br.edu.devmedia.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ConexaoUtil {

	private static ResourceBundle config;
	private static ConexaoUtil conexaoUtil;
	
	private ConexaoUtil() {
		config = ResourceBundle.getBundle("br.edu.devmedia.jdbc/config");
	}
	
	public static ConexaoUtil getInstance() {
		if (conexaoUtil == null) {
			conexaoUtil = new ConexaoUtil();
		}
		return conexaoUtil;
	}
	
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(config.getString("br.edu.devmedia.bd.driver.mysql"));
		return DriverManager.getConnection(
			config.getString("br.edu.devmedia.bd.url.conexao"), 
			config.getString("br.edu.devmedia.bd.usuario"),
			config.getString("br.edu.devmedia.bd.senha")
		);
	}
	
	/*public static void main(String[] args) {
		try {
			System.out.println(getInstance().getConnection());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/*public void listar() throws SQLException {
		Connection connection = null;
		try {
			connection = getConnection();
			
			String sql = "SELECT * FROM pessoas";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
				System.out.println(resultSet.getInt("id"));
				System.out.println(resultSet.getString("nome"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
	}*/
	
	/*public void inserir(PessoasDTO pessoaDTO) throws SQLException {
		Connection connection = null;
		try {
			connection = getConnection();
			
			String sql = "INSERT INTO pessoas (nome) VALUES (?)";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, pessoaDTO.getNome());
			
			statement.execute();
		} catch (Exception e) {
			e.printStackTrace(); 
		} finally {
			connection.close();
		}
	}*/
	
	/*public void atualizar(PessoasDTO pessoaDTO) throws SQLException {
		Connection connection = null;
		try {
			connection = getConnection();
			
			String sql = "UPDATE pessoas SET nome = ? WHERE id = ?";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, pessoaDTO.getNome());
			statement.setInt(2, pessoaDTO.getId());
			
			statement.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
	}*/
	
	/*public void remover(Integer id) throws SQLException {
		Connection connection = null;
		try {
			connection = getConnection();
			
			String sql = "DELETE FROM pessoas WHERE id = ?";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			statement.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.close();
		}
	}*/
	
	/*public static void main(String[] args) {
		
		try {
			PessoasDTO dto = new PessoasDTO();
			dto.setId(6);
			dto.setNome("Carlos Rocha Santos");
			getInstance().atualizar(dto);
			getInstance().listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
}
