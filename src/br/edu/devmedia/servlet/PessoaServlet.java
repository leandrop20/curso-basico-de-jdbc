package br.edu.devmedia.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.devmedia.jdbc.bo.PessoaBO;
import br.edu.devmedia.jdbc.dto.EnderecoDTO;
import br.edu.devmedia.jdbc.dto.EstadoDTO;
import br.edu.devmedia.jdbc.dto.PessoaDTO;

@WebServlet("/pessoa")
public class PessoaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String acao = request.getParameter("acao");
		String nextPage = "home.jsp";
		
		try {
			PessoaBO pessoaBO = new PessoaBO();
			if (acao.equals("cadastrar")) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String nome = request.getParameter("nome");
				String cpf = request.getParameter("cpf");
				String dtNasc = request.getParameter("dtNasc");
				String sexo = request.getParameter("sexo");
				String logradouro = request.getParameter("logradouro");
				String bairro = request.getParameter("bairro");
				String cidade = request.getParameter("cidade");
				String numero = request.getParameter("numero");
				String cep = request.getParameter("cep");
				String estado = request.getParameter("uf");
				
				PessoaDTO pessoaDTO = new PessoaDTO();
				pessoaDTO.setNome(nome);
				pessoaDTO.setCpf(Long.parseLong(cpf));
				pessoaDTO.setDt_nasc(dateFormat.parse(dtNasc));
				pessoaDTO.setSexo(sexo.charAt(0));
				
				EnderecoDTO enderecoDTO = new EnderecoDTO();
				enderecoDTO.setLogradouro(logradouro);
				enderecoDTO.setBairro(bairro);
				enderecoDTO.setCidade(cidade);
				enderecoDTO.setNumero(Long.parseLong(numero));
				enderecoDTO.setCep(Integer.parseInt(cep));
				
				pessoaDTO.setEnderecoDTO(enderecoDTO);
				
				EstadoDTO estadoDTO = new EstadoDTO();
				estadoDTO.setId(Integer.parseInt(estado));
				
				enderecoDTO.setEstadoDTO(estadoDTO);
				
				pessoaBO.cadastrar(pessoaDTO);
				request.setAttribute("msg", "Cadastro Efetuado com sucesso!");
				nextPage = "pessoa?acao=listagem"; 
			} else if (acao.equals("listagem")) {
				nextPage = "list.jsp"; 
				List<PessoaDTO> lista = pessoaBO.listagem();
				request.setAttribute("listaPessoas", lista);
			} else if (acao.equals("editar")) {
				String id = request.getParameter("id");
				PessoaDTO pessoaDTO = pessoaBO.buscaPorId(Integer.parseInt(id));
				request.setAttribute("pessoaDTO", pessoaDTO);
				nextPage = "edicao.jsp";
			} else if (acao.equals("atualizar")) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String idPessoa = request.getParameter("id");
				String idEndereco = request.getParameter("idEndereco");
				String nome = request.getParameter("nome");
				String cpf = request.getParameter("cpf");
				String dtNasc = request.getParameter("dtNasc");
				String sexo = request.getParameter("sexo");
				String logradouro = request.getParameter("logradouro");
				String bairro = request.getParameter("bairro");
				String cidade = request.getParameter("cidade");
				String numero = request.getParameter("numero");
				String cep = request.getParameter("cep");
				String estado = request.getParameter("uf");
				
				PessoaDTO pessoaDTO = new PessoaDTO();
				pessoaDTO.setId(Integer.parseInt(idPessoa));
				pessoaDTO.setNome(nome);
				pessoaDTO.setCpf(Long.parseLong(cpf));
				pessoaDTO.setDt_nasc(dateFormat.parse(dtNasc));
				pessoaDTO.setSexo(sexo.charAt(0));
				
				EnderecoDTO enderecoDTO = new EnderecoDTO();
				enderecoDTO.setId(Integer.parseInt(idEndereco));
				enderecoDTO.setLogradouro(logradouro);
				enderecoDTO.setBairro(bairro);
				enderecoDTO.setCidade(cidade);
				enderecoDTO.setNumero(Long.parseLong(numero));
				enderecoDTO.setCep(Integer.parseInt(cep));
				
				pessoaDTO.setEnderecoDTO(enderecoDTO);
				
				EstadoDTO estadoDTO = new EstadoDTO();
				estadoDTO.setId(Integer.parseInt(estado));
				
				enderecoDTO.setEstadoDTO(estadoDTO);
				
				pessoaBO.atualizar(pessoaDTO);
				
				nextPage = "pessoa?acao=listagem";
			} else if (acao.equals("remover")) {
				String idPessoa = request.getParameter("idPessoa");
				String idEndereco = request.getParameter("idEndereco");
				pessoaBO.remover(Integer.parseInt(idPessoa), Integer.parseInt(idEndereco));
				
				request.setAttribute("msg", "Pessoa exclu�da com sucesso!");
				nextPage = "pessoa?acao=listagem";
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("msg", e.getMessage());
		}
		request.getRequestDispatcher(nextPage).forward(request, response);
	}

}
