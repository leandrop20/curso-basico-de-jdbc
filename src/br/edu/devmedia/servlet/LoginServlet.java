package br.edu.devmedia.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.edu.devmedia.jdbc.Exception.NegocioException;
import br.edu.devmedia.jdbc.bo.EstadoBO;
import br.edu.devmedia.jdbc.bo.LoginBO;
import br.edu.devmedia.jdbc.dto.EstadoDTO;
import br.edu.devmedia.jdbc.dto.LoginDTO;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginBO loginBO = new LoginBO();
		
		String login = request.getParameter("login");
		String senha = request.getParameter("senha");
		HttpSession sessao = request.getSession();
		
		LoginDTO loginDTO = new LoginDTO();
		loginDTO.setLogin(login);
		loginDTO.setSenha(senha);
		
		String nextPage = "home.jsp";
		try {
			boolean resp = loginBO.logar(loginDTO);
			if (!resp) {
				request.setAttribute("msg", "Usu�rio/Senha inv�lidos!");
				nextPage = "login.jsp";
			} else {
				EstadoBO estadoBO = new EstadoBO();
				List<EstadoDTO> lista = estadoBO.listEstados();
				sessao.setAttribute("listaEstados", lista);
			}
		} catch (NegocioException e) {
			nextPage = "login.jsp";
			request.setAttribute("msg", e.getMessage());
			e.printStackTrace();
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
		dispatcher.forward(request, response);
	}
	
}
